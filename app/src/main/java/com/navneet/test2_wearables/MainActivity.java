package com.navneet.test2_wearables;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.particle.android.sdk.cloud.ParticleCloud;
import io.particle.android.sdk.cloud.ParticleCloudSDK;
import io.particle.android.sdk.cloud.ParticleDevice;
import io.particle.android.sdk.cloud.exceptions.ParticleCloudException;
import io.particle.android.sdk.utils.Async;

public class MainActivity extends AppCompatActivity {

    TextView txtMarkingItus,txtTimer,timeSlowsBy;
    Button btnStrt,btnMohammed;


    long totalSeconds = 20;
    long intervalSeconds = 1;
    int  timeNow;
    private CountDownTimer timer;
    String command = "nothing";
    Boolean isMohammed = false;


    // MARK: Debug info
    private final String TAG = "TEST2_WEARABLES";

    // MARK: Particle Account Info
    private final String PARTICLE_USERNAME = "navneetkaur131995@gmail.com";
    private final String PARTICLE_PASSWORD = "Navneet113789";

    // MARK: Particle device-specific info
    private final String DEVICE_ID = "240033000447363333343435";

    // MARK: Particle Publish / Subscribe variables
    private long subscriptionId;

    // MARK: Particle device
    private ParticleDevice mDevice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtMarkingItus = findViewById(R.id.txtMarkingItus);
        txtTimer = findViewById(R.id.txtTimer);
        btnStrt = findViewById(R.id.btnStrt);
        timeSlowsBy = findViewById(R.id.timeSlowsBy);
        btnMohammed = findViewById(R.id.btnMohammad);

        //1. Initialize your connection to the Particle API
        ParticleCloudSDK.init(this.getApplicationContext());

        // 2. Setup your device variable
        getDeviceFromCloud();

        timer = new CountDownTimer(totalSeconds * 1000, intervalSeconds * 1000) {
            @RequiresApi(api = Build.VERSION_CODES.N)
            public void onTick(long millisUntilFinished) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    timeNow = Math.toIntExact((totalSeconds * 1000 - millisUntilFinished) / 1000);
                }
                txtTimer.setText("Elapsed: " + (totalSeconds * 1000 - millisUntilFinished) / 1000);
                whenToDisplay();
            }
            public void onFinish() {
                txtTimer.setText("done!");
            }
        };


        btnStrt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                      timer.start();
                }
        });

        btnMohammed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isMohammed = true;
                intervalSeconds = 50;
                timeSlowsBy.setText("Time slows down by:"+ intervalSeconds);
            }
        });

    }

    public void getDeviceFromCloud() {
        // This function runs in the background
        // It tries to connect to the Particle Cloud and get your device
        Async.executeAsync(ParticleCloudSDK.getCloud(), new Async.ApiWork<ParticleCloud, Object>() {

            @Override
            public Object callApi(@NonNull ParticleCloud particleCloud) throws ParticleCloudException, IOException {
                particleCloud.logIn(PARTICLE_USERNAME, PARTICLE_PASSWORD);
                mDevice = particleCloud.getDevice(DEVICE_ID);
                return -1;
            }

            @Override
            public void onSuccess(Object o) {
                Log.d(TAG, "Successfully got device from Cloud");
                // if you get the device, then go subscribe to events
//                subscribeToParticleEvents();
            }

            @Override
            public void onFailure(ParticleCloudException exception) {
                Log.d(TAG, exception.getBestMessage());
            }
        });
    }

    public void showProgression() {

        Async.executeAsync(ParticleCloudSDK.getCloud(), new Async.ApiWork<ParticleCloud, Object>() {
            @Override
            public Object callApi(@NonNull ParticleCloud particleCloud) throws ParticleCloudException, IOException {
                // put your logic here to talk to the particle
                // --------------------------------------------
                List<String> functionParameters = new ArrayList<String>();
                functionParameters.add(command);
                try {
                    mDevice.callFunction("startProgression", functionParameters);

                } catch (ParticleDevice.FunctionDoesNotExistException e1) {
                    e1.printStackTrace();
                }

                return -1;
            }

            @Override
            public void onSuccess(Object o) {
                // put your success message here
                Log.d(TAG, "Success: Smiley Face shown!!");
            }

            @Override
            public void onFailure(ParticleCloudException exception) {
                // put your error handling code here
                Log.d(TAG, exception.getBestMessage());
            }
        });
    }

    public void whenToDisplay(){

        Log.v("%%%%%%%%%%%%%%%%%%%TIME", ""+timeNow);
        if(timeNow >= 19){
            command = "smilefaceA";
        }
        else if(timeNow == 16){
            command = "smilefaceB";
        }
        else if(timeNow == 8){
            command = "smilefaceC";
        }
        else if(timeNow == 4){
            command = "smilefaceD";
        }
        else if(timeNow == 1){
            command = "smilefaceE";
        }
        showProgression();
    }


}


