
// This #include statement was automatically added by the Particle IDE.
#include <InternetButton.h>
 

#include <math.h>


InternetButton button = InternetButton();


void setup() {
  button.begin();

  Particle.function("startProgression", showProgression);
 

}

int showProgression(String cmd){
    if(cmd == "nothing"){
        button.ledOn(6,255,255,255);
        delay(1000);
    }
    
    else if(cmd == "smilefaceA"){
       button.ledOn(0, 255, 255, 255);
       button.ledOn(1, 255, 255, 255);
       button.ledOn(2, 255, 255, 255);
       button.ledOn(3, 255, 255, 255);
       button.ledOn(9, 255, 255, 255);
       button.ledOn(10, 255, 255, 255);
       button.ledOn(11, 255, 255, 255);
       button.ledOn(5, 255, 255, 255);
       button.ledOn(7, 255, 255, 255);
        
    }
    
    else if (cmd == "smilefaceB"){
       button.ledOn(0, 255, 255, 255);
       button.ledOn(1, 255, 255, 255);
       button.ledOn(2, 255, 255, 255);
    //   button.ledOn(3, 255, 255, 255);
       button.ledOn(10, 255, 255, 255);
       button.ledOn(11, 255, 255, 255);
       button.ledOn(5, 255, 255, 255);
       button.ledOn(7, 255, 255, 255);
    //   button.ledOn(9, 255, 255, 255);
       
    }
    
    else if (cmd == "smilefaceC"){
       button.ledOn(0, 255, 255, 255);
       button.ledOn(1, 255, 255, 255);
    //   button.ledOn(2, 255, 255, 255);
    //   button.ledOn(10, 255, 255, 255);
       button.ledOn(11, 255, 255, 255);
       button.ledOn(5, 255, 255, 255);
       button.ledOn(7, 255, 255, 255);
  
    }
    
    else if(cmd == "smilefaceD"){
       button.ledOn(0, 255, 255, 255);
    //   button.ledOn(1, 255, 255, 255);
       button.ledOn(2, 255, 255, 255);
       button.ledOn(10, 255, 255, 255);
    //   button.ledOn(11, 255, 255, 255);
       button.ledOn(5, 255, 255, 255);
       button.ledOn(7, 255, 255, 255);
   

    }
    
    else if(cmd == "smilefaceE"){
       button.allLedsOn(255,0,0);
       delay(4000);
       button.allLedsOff();
    }
    
    else{
        return - 1;
    }
    
    return 1;
    
}

int DELAY = 200;

void loop() {


  
  
}





